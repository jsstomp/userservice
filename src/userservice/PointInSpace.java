/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userservice;

/**
 *
 * @author jsstomp
 */
public class PointInSpace {

    int x, y, z;

    int calculateDistance(PointInSpace otherPoint) {
        //calculate distance
        int distance = 0;
        distance = (int) Math.sqrt(
                Math.pow(this.x - otherPoint.x, 2)
                + Math.pow(this.y - otherPoint.y, 2)
                + Math.pow(this.z - otherPoint.z, 2));

        return distance;
    }

}
